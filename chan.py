import sys, json, os, urllib
from urlparse import urlparse
import argparse
import time

URL = 'https://a.4cdn.org/'
IMAGE_URL = 'http://i.4cdn.org/'
allowed_types = ['.jpg', '.png', '.gif']


def download():
    response = urllib.urlopen(url)
    try:
        result = json.loads(response.read())
        for post in result['posts']:
            try:
                filename = str(post['tim']) + post['ext']
                if post['ext'] in allowed_types and not os.path.exists(filename):
                    urllib.urlretrieve(IMAGE_URL + board + '/' + filename, filename)
            except KeyError:
                continue
    except ValueError:
        sys.exit('No response. Is the thread deleted?')


def watch():
    while True:
        download()
        time.sleep(60)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("url")
    parser.add_argument("watch", nargs="?", default=0)
    args = parser.parse_args()

    if 'boards.4chan.org' not in args.url:
        sys.exit("You didn't enter a valid 4chan URL")

    split = urlparse(args.url).path.replace('/', ' ').split()
    board, thread = split[0], split[2]
    url = '%s%s/thread/%s.json' % (URL, board, thread)

    try:
        os.mkdir(thread)
        print 'Created directory...'
    except OSError:
        print 'Directory already exists. Continuing. '
        pass

    os.chdir(thread)

    if args.watch == "watch":
        print("Watching thread for new images")
        watch()
    else:
        print("Downloading")
        download()
