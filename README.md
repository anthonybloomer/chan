# chan

 A python script that downloads all images from a 4chan thread.

## Usage

```
python chan.py thread-url
```
